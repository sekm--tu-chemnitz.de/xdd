import matplotlib.pyplot as plt
import src.tuc_colors as tuc
from src.convert import cm2inch

import Measurement

runs = []
runs.append(Measurement.XRD(path='data/20211125_MA0-006.xy', label='Al13Fe4', color=tuc.darkgreen))
runs.append(Measurement.XRD(path='data/20211125_MA0-007.xy', label='Al13Fe4 (P)', color=tuc.red))
runs.append(Measurement.XRD(path='data/20211125_MA0-008.xy', label='Al13Fe4 (7.0)', color=tuc.blue))
runs.append(Measurement.XRD(path='data/20211125_MA0-009.xy', label='Al13Fe4 (P7.0 )', color=tuc.gold))
runs.append(Measurement.XRD(path='data/20211125_MA0-010.xy', label='Al13Fe4 (9.7)', color=tuc.purple))
runs.append(Measurement.XRD(path='data/20211125_MA0-011.xy', label='Al13Fe4 (9.7 P)', color=tuc.orange))

fig, ax = plt.subplots(figsize=cm2inch(20, 10))
props = {'linewidth': 0.5, 'linestyle': '-'}

for run in runs:
    run.plot(ax, props=props)

ax.set_xlabel(r'$\Theta\quad/\quad °$')
ax.set_ylabel('no fucking idea')

ax.set_xlim([40, 60])
ax.tick_params(axis='both', direction='in', right=True, top=True)
fig.subplots_adjust(bottom=0.23, right=0.96, top=0.98)
fig.legend(loc='lower center', frameon=False, ncol=3)
fig.savefig('output/xrd.eps')
fig.show()
