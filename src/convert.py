### Method to convert a tuple of floats into inch
# usecase: fig_size must be given in inch

def cm2inch(*tupl: tuple) -> tuple:
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i / inch for i in tupl[0])
    else:
        return tuple(i / inch for i in tupl)
