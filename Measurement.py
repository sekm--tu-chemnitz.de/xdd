import pandas as pd


class XRD:
    def __init__(self, path, label, color):
        self.data = pd.read_csv(path, delim_whitespace=True, comment='#')
        self.data.columns = ['angle', 'value']
        self.label = label
        self.color = color

    def plot(self, ax, props=None):
        if props is not None:
            ax.plot(self.data['angle'], self.data['value'], color=self.color, label=self.label, **props)
        else:
            ax.plot(self.data['angle'], self.data['value'], color=self.color, label=self.label)
